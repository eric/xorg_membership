from django import forms
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

from .models import Membership, MembershipPeriod


class ProfileForm(forms.Form):
    first_name = forms.CharField(label='First Name', max_length=100)
    last_name = forms.CharField(label='Last Name', max_length=100)

    employer = forms.CharField(label='Employer', help_text="Leave empty for hobbyists",
                               max_length=80, required=False)
    public_statement = forms.CharField(widget=forms.Textarea, label="Public Statement",
                                       help_text="This information will be shown only to other members and admins")

    def save(self, user):
        if self.is_valid():
            user.first_name = self.cleaned_data['first_name']
            user.last_name = self.cleaned_data['last_name']
            user.profile.employer = self.cleaned_data['employer']
            user.profile.public_statement = self.cleaned_data['public_statement']
            user.save()


class MembershipApplicationForm(forms.Form):
    period_id = forms.IntegerField(required=True, widget=forms.HiddenInput())

    user_id = forms.IntegerField(required=True, widget=forms.HiddenInput())

    public_statement = forms.CharField(widget=forms.Textarea, label="Public Statement",
                                       help_text="Update your profile's public statement, which will be "
                                                 "used by administrators to accept/deny your application "
                                                 "and will be seen by other members")

    agree_membership = forms.BooleanField(label="I have read and agreed with the membership agreement", required=True)

    def save(self):
        if self.is_valid() and self.cleaned_data['agree_membership'] is True:
            user = get_object_or_404(User, pk=self.cleaned_data['user_id'])
            period = get_object_or_404(MembershipPeriod, pk=self.cleaned_data['period_id'])

            # Update the user profile
            user.profile.public_statement = self.cleaned_data['public_statement']
            user.save()

            Membership.objects.create(period=period, user_profile=user.profile)

            return True
        else:
            return False
