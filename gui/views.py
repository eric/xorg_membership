from allauth.account.decorators import verified_email_required
from django.shortcuts import render as django_render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import UserPassesTestMixin
from django.utils.safestring import mark_safe
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views import View
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from .models import Link, MembershipPeriod, Membership
from .forms import ProfileForm, MembershipApplicationForm

from gui.context_processors import admin_tasks_count, current_period


# Workaround until I get custom context processors working
def context_processors(context, request):
    context.update(admin_tasks_count(request))
    context.update(current_period(request))
    return context


def render(request, template_path, context):
    context_processors(context, request)
    return django_render(request, template_path, context)


def index(request):
    context = {
        "links": Link.objects.all().order_by("index_position"),
    }
    return render(request, 'gui/index.html', context)


@verified_email_required
def profile(request):
    user = request.user

    if request.method == 'POST':
        form = ProfileForm(request.POST)

        if form.is_valid():
            form.save(user)
            messages.add_message(request, messages.SUCCESS, 'Your profile was updated successfully')
            return HttpResponseRedirect(reverse('index'))
        else:
            messages.add_message(request, messages.ERROR, mark_safe('Your profile is invalid: {}'.format(form.errors)))
            return HttpResponseRedirect(reverse('account-profile'))
    else:
        form = ProfileForm(initial={"first_name": user.first_name,
                                    "last_name": user.last_name,
                                    "employer": user.profile.employer,
                                    "public_statement": user.profile.public_statement})

    return render(request, 'gui/profile.html', {'form': form})


@verified_email_required
def membership_application(request):
    user = request.user

    cur_period = MembershipPeriod.current_period()
    if cur_period is None:
        msg = ("No membership period has been created. If you are an administrator, please create a "
               "period to allow members to apply.")
        return render(request, 'gui/message.html', {"title": "Membership Application",
                                                    "severity": "warning",
                                                    "panel_title": "Can't apply when no membership period exists",
                                                    "message": msg})

    # Do not allow re-applying if an application exists
    if user.profile.membership is not None:
        msg = "You already applied to the period {}. Current status: {}".format(cur_period.short_name,
                                                                                user.profile.membership.status)
        title = "ERROR: An application for the period {} already exists for your user".format(cur_period.short_name)
        return render(request, 'gui/message.html', {"title": "Membership Application",
                                                    "severity": "danger",
                                                    "panel_title": title,
                                                    "message": msg})

    if request.method == 'POST':
        form = MembershipApplicationForm(request.POST)

        if form.is_valid() and form.cleaned_data['user_id'] == user.id and form.save():
            messages.add_message(request, messages.SUCCESS,
                                 'Your application was sent successfully and will be reviewed shortly')
            return HttpResponseRedirect(reverse('index'))
        else:
            messages.add_message(request, messages.ERROR,
                                 mark_safe('Your application is invalid: {}'.format(form.errors)))
            return HttpResponseRedirect(reverse('membership-application'))
    else:
        form = MembershipApplicationForm(initial={"period_id": cur_period.id,
                                                  "user_id": user.id,
                                                  "public_statement": user.profile.public_statement,
                                                  "agree_membership": False})

    return render(request, 'gui/membership_application.html', {'form': form})


class RequiresSuperUserMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_superuser


class ChangeUserStatus(RequiresSuperUserMixin, View):
    def post(self, request, pk, superuser):
        user = get_object_or_404(get_user_model(), pk=pk)
        if user != self.request.user:
            user.is_superuser = superuser
            user.save()

            role = "administrator" if superuser else "user"
            messages.add_message(request, messages.SUCCESS,
                                 "The member '{}' has been made {}".format(user.profile,
                                                                           role))
        else:
            messages.add_message(request, messages.ERROR,
                                 "You cannot change your own access rights")
        return HttpResponseRedirect(request.META.get("HTTP_REFERER", reverse('index')))


@method_decorator(verified_email_required, name='dispatch')
class MembershipPeriodCreateView(RequiresSuperUserMixin, CreateView):
    model = MembershipPeriod
    fields = ['short_name', 'description', 'agreement_url', 'start']
    template_name = "gui/membership_period_create.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context_processors(context, self.request)

    def get_success_url(self):
        return reverse('index')

    def get_initial(self):
        initial = super().get_initial()

        cur_period = MembershipPeriod.current_period()
        if cur_period is not None:
            initial['short_name'] = cur_period.short_name
            initial['description'] = cur_period.description
            initial['agreement_url'] = cur_period.agreement_url
            initial['start'] = cur_period.start

        return initial


@method_decorator(verified_email_required, name='dispatch')
class MembershipApplicationListView(RequiresSuperUserMixin, ListView):
    model = Membership
    paginate_by = 10
    template_name = "gui/membership_approval.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context_processors(context, self.request)

    def get_queryset(self):
        return Membership.pending_memberships()

    def post(self, request, *args, **kwargs):
        params = request.POST

        try:
            ids = [int(id) for id in params.getlist('ids', [])]
            objects = Membership.objects.filter(id__in=ids)

            if params.get("action") == "approve":
                objects.update(approved_on=timezone.now())
                messages.add_message(request, messages.SUCCESS,
                                     'You approved {} membership applications'.format(len(objects)))
            elif params.get("action") == "reject":
                now = timezone.now()

                rejected = []
                for membership in objects:
                    reason = params.get("reject_reason_{}".format(membership.id))
                    if reason is not None and len(reason) > 0:
                        membership.rejected_on = now
                        membership.rejection_reason = reason
                        membership.save()
                        rejected.append(membership)
                    else:
                        msg = "Can't refuse {}'s application without a reason".format(membership.user_profile)
                        messages.add_message(request, messages.ERROR, msg)

                if len(rejected) > 0:
                    messages.add_message(request, messages.SUCCESS,
                                         'You rejected {} membership applications'.format(len(rejected)))

            else:
                messages.add_message(request, messages.ERROR,
                                     "ERROR: Unsupported action...")
        except ValueError:
            messages.add_message(request, messages.ERROR,
                                 "ERROR: One or more IDs are not integers...")

        return HttpResponseRedirect(reverse('membership-approval'))


@method_decorator(verified_email_required, name='dispatch')
class MembersListView(ListView):
    model = Membership
    template_name = "gui/members.html"

    def get_queryset(self):
        period = MembershipPeriod.current_period()
        if period is not None:
            query = period.members.order_by("user_profile__user__last_name")
            return query.select_related('user_profile', 'user_profile__user')
        else:
            return []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context_processors(context, self.request)

    # TODO: Allow changing users to being admins


def about(request):
    return render(request, 'gui/about.html', {})
