= Xorg Membership =

== Quick setup  ==

Here is a quick guide as to how to setup this website for developement:

    $ cp setup-dev.sh.sample setup-dev.sh

    $ python3 -m venv .
    $ source setup-dev.sh
    $ ./manage.py migrate
    $ ./manage.py runserver
